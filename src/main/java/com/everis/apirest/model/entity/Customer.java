package com.everis.apirest.model.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@Entity
@ToString
public class Customer {

		@Id
		private Long id;
		private String name;
		private String lastName;
		private String lastName2;
	
}
