package com.everis.apirest.controller.resource;

import java.util.StringTokenizer;

import lombok.Data;

@Data
public class CustomerReducidoResource {
	
	private String nombre;
	private String apellidoCompleto;
	
	
	private String dosApellidos(int ape){
			
		StringTokenizer strTkns = new StringTokenizer(this.getApellidoCompleto());
		int cont = 0, tam = strTkns.countTokens(), espacios = 0;
		String ape1 = "", ape2= "";
		
		while(strTkns.hasMoreTokens()){
			cont ++;
			if(cont <= tam/2) {
				ape1 +=(espacios < 1)?"":" ";
				ape1 += strTkns.nextToken();
				espacios ++;
			}else{
				ape2 +=(espacios < 100)?"":" ";
				ape2 += strTkns.nextToken(); 
				espacios += 100;		
			}
        }		
		if(ape == 2) {
			ape1=ape2;
		}
		return ape1;
	}
	
	public String primerApellido() {
		return dosApellidos(1);
	}
	
	public String segundoApllido() {
		return dosApellidos(2);
	}
}
