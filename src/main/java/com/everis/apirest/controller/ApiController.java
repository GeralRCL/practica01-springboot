package com.everis.apirest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.everis.apirest.controller.resource.CustomerReducidoResource;

import com.everis.apirest.controller.resource.CustomerResource;
import com.everis.apirest.controller.resource.ProductoResource;
import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.service.CustomerService;
import com.everis.apirest.service.ProductoService;

@RestController
public class ApiController {
	
	
	
	@Autowired
	CustomerService customerService;
	
	
	
	

	/* - - - - -  C R E A R / G U A R D A R   - - - - - - */
	@PostMapping("/customer")
	public CustomerResource addCustomer(@RequestBody CustomerReducidoResource crr) {
		Customer cusadd = new Customer();
		cusadd.setId(customerService.lastId());
		cusadd.setName(crr.getNombre());		
		cusadd.setLastName(crr.primerApellido());
		cusadd.setLastName2(crr.segundoApllido());
		cusadd = customerService.insertar(cusadd);		
		/* ----- RESPONSE ----- */
		CustomerResource customerResource =  new CustomerResource();
		customerResource.setId(cusadd.getId());
		customerResource.setNombre(cusadd.getName());
		customerResource.setApellidoCompleto(cusadd.getLastName()+" "+cusadd.getLastName2());		
		return customerResource;
	}
	

	/* - - - - -  E L I M I NA R   - - - - - - */
	@DeleteMapping("/customer/{idc}")
	public void eliminarCustomer(@PathVariable Long idc) {
		customerService.eliminarCustomer(idc);		
	}
	
	

	/* - - - - -  L I S T A R  - - - - - - */
	@GetMapping("/customer")
	public List<CustomerResource> obtenerCustomer(){
		List<CustomerResource> listado = new ArrayList<>();
		customerService.obtenerCustomer().forEach(customer ->{
			CustomerResource customerResource =  new CustomerResource();
			customerResource.setId(customer.getId());
			customerResource.setNombre(customer.getName());
			customerResource.setApellidoCompleto(customer.getLastName()+"  "+customer.getLastName2());
			listado.add(customerResource);
		});
		return listado;
	}
	


	/* - - - - -  E D I T A R / M O D I F I C A R - - - - - - */
	@PutMapping("/customer/{idc}")
	public void updtCustomer(@RequestBody CustomerReducidoResource crr,@PathVariable Long idc){
		Customer cust = new Customer();
		cust.setId(idc);
		cust.setName(crr.getNombre());		
		cust.setLastName(crr.primerApellido());
		cust.setLastName2(crr.segundoApllido());
		customerService.actualizarCustomer(cust);
	}
	
	

	/* - - - - -  O B T E N E R - U N - C U S T O M E R   - - - - - - */
	@GetMapping("/customer/{idc}")
	public CustomerResource obtenerUnCustomer(@PathVariable Long idc) {
		Customer cust = customerService.obtenerUnCustomer(idc);
		CustomerResource customerResource =  new CustomerResource();
		customerResource.setId(cust.getId());
		customerResource.setNombre(cust.getName());
		customerResource.setApellidoCompleto(cust.getLastName()+" "+cust.getLastName2());		
		return customerResource;
	}
	
	
	
	@Autowired
	ProductoService productoService;

	/* - - - - -  L I S T A R  - - - - - - */
	@RequestMapping(method = RequestMethod.GET, value = "/obtenerProductos")
	public List<ProductoResource> obtenerProductos(){
		List<ProductoResource> listado = new ArrayList<>();
		productoService.obtenerProductos().forEach(producto ->{
			ProductoResource productoResource =  new ProductoResource();
			productoResource.setId(producto.getId());
			productoResource.setName(producto.getName());
			productoResource.setDescripcion(producto.getDescripcion());
			listado.add(productoResource);
		});
		return listado;
	}
	
	
	
}
