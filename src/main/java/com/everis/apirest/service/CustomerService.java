package com.everis.apirest.service;


import com.everis.apirest.model.entity.Customer;

public interface CustomerService {
	
	public Iterable<Customer> obtenerCustomer();
	
	public Customer insertar(Customer customer);
	
	public Long lastId();
	
	public Customer obtenerUnCustomer(Long ids);

	public void actualizarCustomer(Customer cust);

	public void eliminarCustomer(Long idc);
	
}
