package com.everis.apirest.service;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.apirest.model.entity.Customer;
import com.everis.apirest.model.repository.CustomerRepository;

@Service
public class CustomerServiceImpl  implements CustomerService {
	
	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Iterable<Customer> obtenerCustomer() {
		return customerRepository.findAll();
	}

	@Override
	public Customer insertar(Customer customer) {
		customerRepository.save(customer);
		return customerRepository.findById(customer.getId()).get();
	}

	@Override
	public Long lastId() {
		Long nid = customerRepository.count() + 1;		
		boolean ok_nid = true;
		while(ok_nid) {
			Optional<Customer> cust = customerRepository.findById(nid);
			if(cust.isPresent()) {
				nid = nid + 1;
			}else {
				ok_nid = false;
			}
		}
		return nid;
	}

	@Override
	public Customer obtenerUnCustomer(Long ids) {
		Optional<Customer> cuso = customerRepository.findById(ids);
		Customer cus = cuso.get();
		return cus;
	}

	@Override
	public void actualizarCustomer(Customer cust) {
		if(customerRepository.existsById(cust.getId())) {
			customerRepository.save(cust);
		}else{
			System.out.println("  >> Customer no encontrado.");
		}
	}

	@Override
	public void eliminarCustomer(Long idc) {
		if(customerRepository.existsById(idc)) {
			customerRepository.deleteById(idc);
		}else{
			System.out.println("  >> Customer no encontrado.");
		}
	}

}
